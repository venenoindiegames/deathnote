extends KinematicBody2D

const GRAVITY = 1500.0

const WALK_FORCE = 600
const WALK_MIN_SPEED = 10
const WALK_MAX_SPEED = 300
const STOP_FORCE = 1300
const JUMP_SPEED = 700
const JUMP_MAX_AIRBORNE_TIME = 0.2
const FLOOR_ANGLE_TOLERANCE = 40

const SLIDE_STOP_VELOCITY = 1.0
const SLIDE_STOP_MIN_TRAVEL = 1.0

var velocity = Vector2()
var on_air_time = 100
var jumping = false

var prev_jump_pressed = false

var new_anim = ""
var cur_anim = ""

var alive = true
var playing = true

onready var death_timer = get_node("death_timer")
onready var pre_death_text = preload("res://asstes/scenes/death_text.tscn")
onready var pre_victory = preload("res://asstes/scenes/victory.tscn")

var body
var head
var front_arm
var back_arm
var front_leg
var back_leg
var drop

signal reload

func _fixed_process(delta):
	var force = Vector2(0,GRAVITY)

	var walk_left = Input.is_action_pressed("move_left")
	var walk_right = Input.is_action_pressed("move_right")
	var jump = Input.is_action_pressed("jump")
	var chao = get_node("ray_dir").is_colliding() || get_node("ray_esq").is_colliding()
	var stop = true
	
	if(!alive and chao):
		force = Vector2(0, 0)
		new_anim = "death"
		
	if(walk_left and alive and playing):
		if(velocity.x <= WALK_MIN_SPEED and velocity.x > -WALK_MAX_SPEED):
			force.x -= WALK_FORCE
			get_node("sprites").set_scale(Vector2(-1,1))
			stop = false
			if (chao and not jumping):
				#get_node("anim").set_speed(2)
				new_anim = "running"
				audio_control.play_steps()
	elif(walk_right and alive and playing):
		if(velocity.x >= -WALK_MIN_SPEED and velocity.x < WALK_MAX_SPEED):
			force.x += WALK_FORCE
			get_node("sprites").set_scale(Vector2(1,1))
			stop = false
			if (chao and not jumping):
				#get_node("anim").set_speed(2)
				new_anim = "running"
				audio_control.play_steps()

	if (stop and alive and playing):
		var vsign = sign(velocity.x)
		var vlen = abs(velocity.x)
		vlen -= STOP_FORCE*delta
		if(vlen < 0):
			vlen = 0
			if (chao and not jumping and alive):
				new_anim = "idle"
		velocity.x = vlen*vsign
		

	velocity += force*delta
	
	var motion = velocity*delta
	
	motion = move(motion)
	
	var floor_velocity = Vector2()
	
	if(is_colliding() and alive and playing):
		var normal = get_collision_normal()
		
		if(rad2deg(acos(normal.dot(Vector2(0, -1)))) < FLOOR_ANGLE_TOLERANCE):
			on_air_time = 0
			floor_velocity = get_collider_velocity()
		
		if(on_air_time == 0 and force.x == 0 and get_travel().length() < SLIDE_STOP_MIN_TRAVEL and abs(velocity.x) < SLIDE_STOP_VELOCITY and get_collider_velocity() == Vector2()):
			revert_motion()
			velocity.y = 0.0
		else:
			motion = normal.slide(motion)
			velocity = normal.slide(velocity)
			move(motion)
	
	if (jumping and velocity.y > 0):
		jumping = false
		if(playing and alive and !chao):
			new_anim = "fall"
	
	if (velocity.y > 0):
		if(playing and alive and !chao):
			new_anim = "fall"
	
	if (on_air_time < JUMP_MAX_AIRBORNE_TIME and jump and not prev_jump_pressed and not jumping and alive and playing):
		velocity.y = -JUMP_SPEED
		jumping = true
		new_anim = "jump"
		audio_control.play_jumps()
	
	on_air_time += delta
	prev_jump_pressed = jump
	
	if (cur_anim != new_anim):
		get_node("anim").play(new_anim)
		cur_anim = new_anim
		if(new_anim == "running"):
			build_player_running()
		elif(new_anim == "idle"):
			build_player_idle()
		elif(new_anim == "jump"):
			build_player_idle()
		elif(new_anim == "fall"):
			build_player_idle()
		elif(new_anim == "death"):
			build_player_dead()
		elif(new_anim == "victory"):
			build_player_win()

func death():
	if(alive):
		alive = false
		new_anim = "death"
		audio_control.play_death()
		get_node("body").queue_free()
		get_node("reload_timer").start()
		velocity = Vector2(0,0)
		death_timer.stop()

func death_text():
	var death_text = pre_death_text.instance()
	death_text.set_pos(get_global_pos())
	death_text.rand_death = randi()%10+1
	get_tree().get_root().get_node("main").add_child(death_text)

func _ready():
	set_fixed_process(true)
	randomize()
	death_timer.set_wait_time(rand_range(5, 10))
	death_timer.start()
	var scene = get_node("/root/main/scenarios").get_child(0)
	self.connect("reload", scene, "reset_player")
	load_playertex.random_numbers()
	get_sprites()

func build_player_win():
	head.set_texture(load_playertex.load_random_head_win())
	body.set_texture(load_playertex.load_body())
	front_arm.set_texture(load_playertex.load_right_arm())
	back_arm.set_texture(load_playertex.load_left_arm())
	front_leg.set_texture(load_playertex.load_right_leg())
	back_leg.set_texture(load_playertex.load_left_leg())
	drop.hide()

func build_player_dead():
	head.set_texture(load_playertex.load_random_head_dead())
	body.set_texture(load_playertex.load_body())
	front_arm.set_texture(load_playertex.load_right_arm())
	back_arm.set_texture(load_playertex.load_left_arm())
	front_leg.set_texture(load_playertex.load_right_leg())
	back_leg.set_texture(load_playertex.load_left_leg())
	drop.hide()

func build_player_idle():
	head.set_texture(load_playertex.load_random_head_idle())
	body.set_texture(load_playertex.load_body())
	front_arm.set_texture(load_playertex.load_right_arm())
	back_arm.set_texture(load_playertex.load_left_arm())
	front_leg.set_texture(load_playertex.load_right_leg())
	back_leg.set_texture(load_playertex.load_left_leg())
	drop.hide()

func build_player_running():
	head.set_texture(load_playertex.load_random_head_running())
	body.set_texture(load_playertex.load_body())
	front_arm.set_texture(load_playertex.load_front_arm_running())
	back_arm.set_texture(load_playertex.load_back_arm_running())
	front_leg.set_texture(load_playertex.load_front_leg_running())
	back_leg.set_texture(load_playertex.load_back_leg_running())
	drop.show()
	
func get_sprites():
	body = get_node("sprites/body_sprite")
	head = get_node("sprites/body_sprite/head")
	front_arm = get_node("sprites/body_sprite/front_arm")
	back_arm = get_node("sprites/body_sprite/back_arm")
	front_leg = get_node("sprites/body_sprite/front_leg")
	back_leg = get_node("sprites/body_sprite/back_leg")
	drop = get_node("sprites/body_sprite/head/drop")

func player_win():
	if(playing):
		death_timer.stop()
		var victory = pre_victory.instance()
		victory.set_pos(Vector2())
		get_tree().get_root().get_node("main").add_child(victory)
		new_anim = "victory"
		playing = false
		velocity = Vector2(0,0)
		audio_control.player_win()

func flip_player():
	if(get_node("sprites").get_scale() == Vector2(-1,1)):
		get_node("sprites").set_scale(Vector2(1,1))
	else:
		get_node("sprites").set_scale(Vector2(-1,1))

func _on_death_timer_timeout():
	death()

func _on_reload_timer_timeout():
	emit_signal("reload")
	get_node("reload_timer").queue_free()
