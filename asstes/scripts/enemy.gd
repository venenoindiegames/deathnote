extends Sprite

signal win
var player

func _on_area_body_enter( body ):
	player = get_tree().get_root().get_node("main/" + body.get_name())
	self.connect("win", player, "player_win")
	emit_signal("win")
