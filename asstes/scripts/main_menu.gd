extends Control

var main = "res://asstes/scenes/main.tscn"
var instructions = "res://asstes/scenes/instructions.tscn"
var credits = "res://asstes/scenes/credits.tscn"

func _ready():
	set_process(true)

func _process(delta):
	audio_control.play_music()

func _on_btn_start_pressed():
	audio_control.play_click()
	transition.fade_to(main)
	get_node("canvas/horizontal_container/VBoxContainer/btn_start").set_disabled(true)

func _on_btn_instruction_pressed():
	audio_control.play_click()
	transition.fade_to(instructions)
	get_node("canvas/horizontal_container/VBoxContainer/btn_instruction").set_disabled(true)

func _on_btn_quit_pressed():
	get_tree().quit()

func _on_btn_credits_pressed():
	audio_control.play_click()
	transition.fade_to(credits)
	get_node("canvas/horizontal_container/VBoxContainer/btn_credits").set_disabled(true)


func _on_btn_en_pressed():
	audio_control.play_click()
	TranslationServer.set_locale("en")
	get_tree().reload_current_scene()

func _on_btn_pt_BR_pressed():
	audio_control.play_click()
	TranslationServer.set_locale("pt_BR")
	get_tree().reload_current_scene()
