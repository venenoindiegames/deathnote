extends Node

onready var music_player = get_node("music_player")
onready var SFX_player = get_node("SFX_player")

var steps = false
var song1
var song2

func _ready():
	song1 = load("res://asstes/sounds/music/song1.ogg")
	song2 = load("res://asstes/sounds/music/song2.ogg")

func play_music():
	if(!music_player.is_playing()):
		if(music_player.get_stream() == song1):
			music_player.set_stream(song2)
		else:
			music_player.set_stream(song1)
	else: return
	music_player.play()

func play_steps():
	if(!SFX_player.is_active()):
		SFX_player.play("steps1", true)
		steps = true

func stop_all():
	if(steps):
		SFX_player.stop_all()

func play_win():
	SFX_player.play("win1")

func player_win():
	SFX_player.play("win")

func play_jumps():
	SFX_player.play("jump" + str(randi()%2+1))
	
func play_death():
	SFX_player.play("death" +str(randi()%3+1))

func play_click():
	SFX_player.play("click")