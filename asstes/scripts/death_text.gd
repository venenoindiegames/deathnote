extends Node2D

onready var label = get_node("label")
var timer
var wait_time = 0.02
var rand_death

var show = true
var shown = false

func _ready():
	label.hide()
	timer = Timer.new()
	timer.set_wait_time(wait_time)
	add_child(timer)
	timer.connect("timeout", self, "on_timer_timeout")

func on_timer_timeout():
	label.set_visible_characters(label.get_visible_characters() + 1)
	if label.get_visible_characters() == label.get_total_character_count():
		timer.stop()
		get_node("exit_timer").start()

func _on_area_body_enter( body ):
	show_text()
	
func show_text():
	if(show):
		label.set_text(tr("DEATH" + str(rand_death)))
		label.show()
		label.set_visible_characters(0)
		timer.start()
		show = false

func _on_exit_timer_timeout():
	label.hide()

