extends Node

var scenario
var paused = false

func _ready():
	set_process(true)
	instance_scenario()

func _process(delta):
	audio_control.play_music()

func instance_scenario():
	scenario = load_scenario.randomScenario().instance()
	scenario.set_pos(Vector2())
	get_node("scenarios").add_child(scenario)
	
func reload_scene():
	get_tree().reload_current_scene()

