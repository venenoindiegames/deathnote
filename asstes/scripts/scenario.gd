extends Node2D

onready var player_pos = get_node("player_pos")
onready var enemy_pos = get_node("enemy_pos")
onready var main = get_tree().get_root().get_node("main")

onready var pre_player = preload("res://asstes/scenes/player.tscn")
onready var pre_enemy = preload("res://asstes/scenes/enemy.tscn")

func _ready():
	instance_player_and_enemy()


func instance_player_and_enemy():
	var enemy = pre_enemy.instance()
	enemy.set_pos(enemy_pos.get_pos())
	main.add_child(enemy)
	var player = pre_player.instance()
	player.set_pos(player_pos.get_pos())
	main.add_child(player)

func reset_player():
	var player = pre_player.instance()
	player.set_pos(player_pos.get_pos())
	main.add_child(player)