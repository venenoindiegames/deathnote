extends Node

var rand_head
var rand_body
var rand_arm
var rand_leg

var idle_head = []
var win_head = []
var dead_head = []
var bodies = []
var left_arms = []
var right_arms = []
var left_legs = []
var right_legs = []
#running
var running_heads = []
var running_front_arms = []
var running_front_legs = []
var running_back_arms = []
var running_back_legs = []

#heads dir
const HEAD_RUNNING_DIR = "res://asstes/sprites/player/running/heads/"
const HEADS_IDLE_DIR = "res://asstes/sprites/player/heads/idle/"
const HEADS_DEAD_DIR = "res://asstes/sprites/player/heads/dead/"
const HEADS_WIN_DIR = "res://asstes/sprites/player/heads/win/"
#arms dir
const FRONT_ARMS_RUNNING_DIR = "res://asstes/sprites/player/running/front_arms/"
const BACK_ARMS_RUNNING_DIR = "res://asstes/sprites/player/running/back_arms/"
const RIGHT_ARMS_DIR = "res://asstes/sprites/player/right_arms/"
const LEFT_ARMS_DIR = "res://asstes/sprites/player/left_arms/"
#legs dir
const BACK_LEGS_RUNNING_DIR = "res://asstes/sprites/player/running/back_legs/"
const FRONT_LEGS_RUNNING_DIR = "res://asstes/sprites/player/running/front_legs/"
const LEFT_LEGS_DIR = "res://asstes/sprites/player/left_legs/"
const RIGHT_LEGS_DIR = "res://asstes/sprites/player/right_legs/"
#bodies dir
const BODIES_DIR = "res://asstes/sprites/player/bodies/"

func _ready():
	load_all()
	randomize()

func random_numbers():
	rand_head = randi()%running_heads.size()
	rand_body = randi()%bodies.size()
	rand_arm = randi()%running_back_arms.size()
	rand_leg = randi()%running_back_legs.size()

func load_all():
	load_tex(HEAD_RUNNING_DIR, running_heads)
	load_tex(HEADS_IDLE_DIR, idle_head)
	load_tex(HEADS_DEAD_DIR, dead_head)
	load_tex(HEADS_WIN_DIR, win_head)
	load_tex(BODIES_DIR, bodies)
	load_tex(FRONT_ARMS_RUNNING_DIR, running_front_arms)
	load_tex(FRONT_ARMS_RUNNING_DIR, running_back_arms)
	load_tex(FRONT_LEGS_RUNNING_DIR, running_front_legs)
	load_tex(BACK_LEGS_RUNNING_DIR, running_back_legs)
	load_tex(RIGHT_ARMS_DIR, right_arms)
	load_tex(LEFT_ARMS_DIR, left_arms)
	load_tex(RIGHT_LEGS_DIR, right_legs)
	load_tex(LEFT_LEGS_DIR, left_legs)

func load_tex(DIR, load_array):
	var dir = Directory.new()
	dir.change_dir(DIR)
	dir.list_dir_begin()
	
	var heads_file = dir.get_next()
	
	while (heads_file != ""):
		var sprite = load(DIR + heads_file)
		if sprite:
			load_array.append(sprite)
		heads_file = dir.get_next()

#load heads
func load_random_head_idle():
	return idle_head[rand_head]

func load_random_head_dead():
	return dead_head[rand_head]

func load_random_head_win():
	return win_head[rand_head]

func load_random_head_running():
	return running_heads[rand_head]
#load running
func load_front_arm_running():
	return running_front_arms[rand_arm]

func load_back_arm_running():
	return running_back_arms[rand_arm]

func load_front_leg_running():
	return running_front_legs[rand_leg]

func load_back_leg_running():
	return running_back_legs[rand_leg]
#load
func load_body():
	return bodies[rand_body]
func load_left_arm():
	return left_arms[rand_arm]

func load_right_arm():
	return right_arms[rand_arm]

func load_left_leg():
	return left_legs[rand_leg]

func load_right_leg():
	return right_legs[rand_leg]